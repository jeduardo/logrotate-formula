describe package('logrotate') do
  it { should be_installed }
end

describe file('/etc/logrotate.d') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/logrotate.conf') do
 it { should exist }
 it { should be_owned_by 'root' }
end

describe file('/etc/logrotate.d/syslog') do
 it { should exist }
 it { should be_owned_by 'root' }
end
